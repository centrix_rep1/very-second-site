#!/usr/bin/env python
import random
import requests
from bs4 import BeautifulSoup
from flask import Flask, render_template, url_for, request


app = Flask(__name__)

@app.route('/', methods=["GET", "POST"])
def index():

    bash = requests.get('http://bash.im/random')
    bash.encoding = 'windows-1251'
    soup = BeautifulSoup(bash.content.decode('windows-1251'), 'html.parser')
    quotes = soup.find_all('div', class_='text')
    random_quote = random.choice(quotes)
    return render_template("index.html", random_bash=random_quote.prettify())
    #return render_template("index.html", random_bash=bash.content.decode('windows-1251'))




if __name__ == '__main__':
    app.run(debug=True)
